<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Jobs_Apply;
use App\Category;
class JobController extends Controller
{
    public function showAllJob(){
        $categories = Category::all();
        $jobs = DB::table('jobs')->paginate(3);
        return view('Job.showAllJob', ['jobs' => $jobs,'categories' => $categories]);

    }

    public function jobDetail($id){
        $jobs= DB::table('jobs')->where('id','=',$id)->get();
        return view("Job.jobDetail",["jobs" => $jobs,"id"=>$id]);
    }
    public function sendApply(){
        return view('Job.sendApply');
    }
    public function sendApply_post(Request $request,$id){
        $Apply_p= new Jobs_Apply();
        $Apply_p->user_id = Auth::user()->id;
        $Apply_p->job_id = $id;
        $Apply_p->description = $request->description;
        $Apply_p->save();
        return "saved ";
    }

}
