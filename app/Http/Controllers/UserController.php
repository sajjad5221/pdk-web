<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use DB;
use Flash;
use App\User;
use App\Project;
use App\Job;
use App\Jubs_Apply;
use App\ProjectVolunteer;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function showPanel(){
      $user = Auth::user();
      return view('User.userPanel');
    }
    public function showProfile(){
      return view('User.showProfile');
    }
    public function showApplicantProject(){
      $a_projects = DB::table("projects")
      ->join('users','projects.user_id','=','users.id')->where('employer_id','=',Auth::user()->id)->get();
      return view('User.showApplicantProject',["a_projects" => $a_projects]);
    }
    public function showGivenProject(){
      $g_projects = DB::table('projects')
      ->join('users', 'projects.employer_id', '=', 'users.id')->where('user_id','=',Auth::user()->id)->get();
      return view('User.showGivenProject',["g_projects" => $g_projects]);
    }
    public function editProfile(Request $request){
      return view('User.editProfile');
     //return $request->all();
    }
    public function editProfile_post(Request $request){
    $user = Auth::user();
    $data = $request->validate([
        'name' => 'required',
        'email' => 'required|email|unique:users',
    ]);
    $user->fill($data);
    $user->save();
    Flash::message('Your account has been updated!');
    return back();
    
    }
    public function changePassword(){
     return view('User.changePassword');
    }
    public function changePassword_post(Request $requeset){
      $user = Auth::user();
      $messages = [
        'current-password.required' => 'Please enter current password',
        'password.required' => 'Please enter password',
      ];
    
      
      $user->password = bcrypt($requeset->password);
      $user->save();
      return "Changed";

      /*if($user->password == $requeset->prePass){

      }
      else{      return $requeset->password;

        Flash::message('Previus password is wrong!');
      }*/
      }
            
    public function userPanel(){
     
        $user = Auth::user();
        $projects = DB::table('projects')->where('user_id','=',Auth::user()->id)->get();
        return view('User.User_P',["user" => $user,"projects"=> $projects]);

      }
      public function addProject(){
        return view('User.addProject');
      }
      public function addProject_post(Request $request){
        $project = new Project;
        $project->title = $request->title;
        $project->description = $request->description;
        $project->price = $request->price;
        $project->employer_id = Auth::user()->id;
        $project->save();
        return "saved";
      }
      public function addJob(){
        return view('User.addJob');
      }
      public function addJob_post(Request $request){
        $job = new Job;
        $job->title = $request->title;
        $job->description = $request->description;
        $job->amount = $request->amount;
        $job->employer_id = Auth::user()->id;
        $job->save();
        return "saved";
      }
      public function showJob(){
        $jobs = DB::table('jobs_apply')
        ->join('jobs', 'jobs.id', '=', 'jobs_apply.job_id')->where('user_id','=',Auth::user()->id)->get();
      
        return view('User.showJob',["jobs" => $jobs]);
      }
      public function projectVolunteers($id){
        $volunteers = DB::table('projects_apply')
        ->join('users', 'users.id', '=', 'projects_apply.user_id')->where('project_id','=',$id)->get();
      
        return view('User.projectVolunteers',["volunteers" => $volunteers]);
      }
      public function acceptRequest($id){
        $user = DB::table('users')->where('id','=',$id)->get();
        return $user;
      }
    }