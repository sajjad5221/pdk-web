<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Category;   
use App\Advertising; 
use Auth;  
class AdvertisingController extends Controller
{
    public function makeAdvertising(){
        $categories = Category::all();
        return view('Advertising.makeAdvertising', ['categories' => $categories]);
    }
    public function makeAdvertising_post(Request $request){
        
        $advertising = new Advertising;
        $advertising->title = $request->title;
        $advertising->description = $request->about;
        $advertising->user_id = Auth::user()->id;
        //$advertising->tags = $request->selectedTags;
        $advertising->tags = json_encode($request->selectedTags);
        $advertising->save();
        dump(json_encode($request->selectedTags));
        dump(($request->selectedTags));
                //$advertising->image_id = Auth::user()->id;

    }
    function showAllAdvertising(){
        $categories = Category::all();
        $advertisings = DB::table('advertising')->paginate(3);
        return view('Advertising.showAllAdvertising', ['advertisings' => $advertisings,'categories' => $categories]);
    }
    function advertisingDetail($id){
        $advertising= DB::table('advertising')->where('id','=',$id)->get();
        
        return view("Advertising.advertisingDetail",["advertising" => $advertising,"id"=>$id]);
    }
}
