<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApplicantProject;
use App\Category;

class AdminController extends Controller
{
    public function show_applicant_project(){
        $a_project = ApplicantProject::all();
        foreach($a_project as $a ){
            return view("Admin/show_applicant_project");
        }
    }
    public function addCategory(){
        $categories = Category::all();
       return view("Admin.addCategory",["categories" => $categories]);
    }
    public function addCategory_post(Request $request){
        $category = new Category;
        $category->parent_id=$request->parent_id;
        $category->name=$request->name;
        $category->save();
        return ("well done!");
    }
}