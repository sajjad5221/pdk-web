<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tutorial;
use App\Category;
use Auth;
use DB;
class TutorialController extends Controller
{
    public function makeTutorial(){
        $categories = Category::all();
        return view('Tutorial.makeTutorial', ['categories' => $categories]);
    }
    public function makeTutorial_post(Request $request){
        
        $tutorial = new Tutorial;
        $tutorial->title = $request->title;
        $tutorial->description = $request->about;
        $tutorial->user_id = Auth::user()->id;
        //$advertising->tags = $request->selectedTags;
        $tutorial->tags = json_encode($request->selectedTags);
        $tutorial->save();
        
                //$advertising->image_id = Auth::user()->id;

    }

    function showAllTutorial(){
        $categories = Category::all();

        $tutorials = DB::table('tutorials')->paginate(3);
        return view('Tutorial.showAllTutorial', ['tutorials' => $tutorials,'categories' => $categories]);
    }
    function tutorialDetail($id){
        $tutorials= DB::table('tutorials')->where('id','=',$id)->get();
        
        return view("Tutorial.tutorialDetail",["tutorials" => $advertising,"id"=>$id]);
    }
}
