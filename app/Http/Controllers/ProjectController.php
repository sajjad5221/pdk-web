<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Project;
use App\Projects_Apply;
use App\Category;
class ProjectController extends Controller
{
    public function projectDetail($id){
        $a_project= DB::table('projects')->where('id','=',$id)->get();
        
        return view("Project.Project_Detail",["a_project" => $a_project,"id"=>$id]);
    }
    public function projectDetail_post(Request $request){
        $a_project= new ApplicantProject();
        $a_project->user_id = Auth::user()->id;
        $a_project->save();
        return "saved data";
    }
    public function showAllProject(){
        $categories = Category::all();
        $projects = DB::table('projects')->paginate(3);
        return view('Project.showAllProject', ['projects' => $projects,'categories' => $categories]);
    }
    public function showAllProject_post(Request $request){
        $searchedTags = $request->tags;
        // $categories = Category::all();
        // $projects = DB::table('projects')->where('tags','=',$id)->paginate(3);
       // return view('Project.showAllProject', ['projects' => $projects,'categories' => $categories]);
       $i=0;
       while($categoriesi != strlen($searchedTags)){
            /*for($searchedTags!=','){
                $tag += $searchedTags; 
            }*/
       }
       return strlen($searchedTags);
    }
    public function sendApply(){
        return view('Project.sendApply');
    }
    public function sendApply_post(Request $request,$id){
        $Apply_p= new Projects_Apply();
        $Apply_p->user_id = Auth::user()->id;
        $Apply_p->project_id = $id;
        $Apply_p->suggested_price = $request->price;
        $Apply_p->suggested_day = $request->day;
        $Apply_p->description = $request->description;
        $Apply_p->save();
        return "saved ";
    }
    public function makeProject(){
        $categories = Category::all();
        //return $categories->parent_id;
        return view('Project.makeProject', ['categories' => $categories]);
    }
    public function makeProject_post(Request $request){
        $project= new Project();
        $project->title = $request->title;
        $project->employer_id = Auth::user()->id;
        $project->price = $request->price;
        $project->description = $request->description;
        //$photo=$request->files;
         /*foreach($photo as $index => $p) {
             return $p;
             //return $request->files[$index]->getClientOrginalName();
         }*/
        //$path = $request->file('files')->storeAs('Files/Project',"adsd");
        $project->save();
        // return $path;
    }

}