<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectVolunteer extends Model
{
    protected $fillable = [
        'project_id', 'user_id', 'suggested_price','suggested_day','description'
    ];
    public function user()
    {
        return $this->hasOne('App\User');
    }
    public function job()
    {
        return $this->hasOne('App\Project');
    }
}
