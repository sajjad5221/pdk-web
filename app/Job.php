<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'title', 'description','amount'
    ];
    // public function user()
    // {
    //     return $this->hasOne('App\User');
    // }
    public function employer()
    {
        return $this->hasOne('App\Employer');
    }
}
