<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertising extends Model
{
    protected $fillable = [
        'image_id', 'user_id', 'title','description','tags'
    ];
    public function user()
    {
        return $this->hasOne('App\User');
    }
    
    protected $table = "advertising";

}
