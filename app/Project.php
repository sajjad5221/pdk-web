<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'title', 'description', 'status','price','tags'
    ];
    public function user()
    {
        return $this->hasOne('App\User');
    }
    public function employer()
    {
        return $this->hasOne('App\Employer');
    }
}
