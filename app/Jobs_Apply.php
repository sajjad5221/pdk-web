<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs_Apply extends Model
{
    protected $fillable = [
        'job_id', 'user_id', 'status'
    ];
    public function user()
    {
        return $this->hasOne('App\User');
    }
    public function job()
    {
        return $this->hasOne('App\Job');
    }
    protected $table = "jobs_apply";
}
