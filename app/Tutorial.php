<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model
{
    protected $fillable = [
         'user_id', 'title','description','tags'
    ];
    public function user()
    {
        return $this->hasOne('App\User');
    }
    
}
