@extends('layouts.main')

<!doctype html>
<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/HomeCssFile.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/bootstrap.js"></script>

</head>

<body >
     <div class="home-header" style="font-family:'Vazir'">
<div class="container-fluid">
        <div class="row project-title">
            <div class="col-md-3 header-gif-1">
                <img src="/img/home/1.gif" alt="index gif">
            </div>
            <div class="col-md-3 header-gif-2">
                <img src="/img/home/2.gif" alt="index gif">
            </div>
            <div class="col-md-3 header-gif-3">
                <img src="/img/home/3.gif" alt="index gif">
            </div>
            <div class="col-md-offset-2 col-md-4 pull-right">
                <h1 class="name">برای پروژه خود نیروی متخصص استخدام کنید</h1>
                <br>
                <button class="btn btn-success">همین حالا شروع کنید</button>
            </div>
            {{-- <h3 class="create">ایجاد شده توسط <a href="/users/#">dhakimi</a></h3>
            <div class="send-suggest">
                <a href="/project/sendApply/"><button class="btn btn-danger " style="font-size:25px;">ارسال پیشنهاد</button></a>
            </div> --}}

        </div>
    </div>
    </div> 
    <div class="container" style="text-align:center">
        <h1>آخرین پروژه ها</h1>
    </div>
    <div class="container" >

    <div class="row " >
        <div class="col-md-3  detail-box" >
            <p class="header">رفع ایراد اپلیکیشن اندروید برای اتصال به درگاه زرین پال </p>
            <p class="user">توسط ali </p>
            <hr>
            <div class="row" >
                <div class="preffer col-md-3">
                    <div class="number">
                        <p>15</p>
                    </div>
                    <div class="name">
                        <p>پیشنهاد</p>
                    </div>
                </div>

                <div class="price col-md-3 col-md-offset-2" >
                        <div class="vertical-line "></div>

                        <div class="number ">
                            <p>2,500,000</p>
                        </div>
                        <div class="name" style="margin-left:20px;">
                            <p>(IRR)مبلغ</p>
                        </div>

            </div>
        </div>
        <div class="do-by" dir="rtl">
            <p> انجام شده توسط smartmob
                </p>

        </div>
    </div>

            <div class="col-md-3 col-md-offset-1 detail-box" >
                <p class="header">رفع ایراد اپلیکیشن اندروید برای اتصال به درگاه زرین پال </p>
                <p class="user">توسط ali </p>
                <hr>
                <div class="row" >
                    <div class="preffer col-md-3">
                        <div class="number">
                            <p>15</p>
                        </div>
                        <div class="name">
                            <p>پیشنهاد</p>
                        </div>
                    </div>
    
                    <div class="price col-md-3 col-md-offset-2" >
                            <div class="vertical-line "></div>
    
                            <div class="number ">
                                <p>2,500,000</p>
                            </div>
                            <div class="name" style="margin-left:20px;">
                                <p>(IRR)مبلغ</p>
                            </div>
    
                </div>
            </div>
            <div class="do-by" dir="rtl">
                <p> انجام شده توسط smartmob
                    </p>
    
            </div>
        </div>


                <div class="col-md-3 col-md-offset-1 detail-box" >
                    <p class="header">رفع ایراد اپلیکیشن اندروید برای اتصال به درگاه زرین پال </p>
                    <p class="user">توسط ali </p>
                    <hr>
                    <div class="row" >
                        <div class="preffer col-md-3">
                            <div class="number">
                                <p>15</p>
                            </div>
                            <div class="name">
                                <p>پیشنهاد</p>
                            </div>
                        </div>
        
                        <div class="price col-md-3 col-md-offset-2" >
                                <div class="vertical-line "></div>
        
                                <div class="number ">
                                    <p>2,500,000</p>
                                </div>
                                <div class="name" style="margin-left:20px;">
                                    <p>(IRR)مبلغ</p>
                                </div>
        
                    </div>
                </div>
                <div class="do-by" dir="rtl">
                    <p> انجام شده توسط smartmob
                        </p>
        
                </div>
            </div>
        </div>

            
        </div>
    </hr>




</body>
{{--

<body>

    <div class="main" style="padding-top:100px;">
        <center>
            <div dir="rtl">
                <h2 style="color: dodgerblue">آخرین آموزش ها</h2>
            </div>

        </center>
        <div class="container" dir="rtl">
            <div class="row">
                <div class="col-sm-4 ">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">آیتم اول</div>
                        <div class="panel-body"><img src="/img/icon-1.png" class="img-responsive" style="width:100%"
                                alt="Image"></div>
                        <div class="panel-footer ">
                            <p>آموزش حل معادلات لاپلاس به روش آسان آموزش حل معادلات لاپلاس به روش آسان</p>
                            <a href="/project/Detail/1"><button class="btn btn-danger">نمایش</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">آیتم اول</div>
                        <div class="panel-body"><img src="/img/icon-1.png" class="img-responsive" style="width:100%"
                                alt="Image"></div>
                        <div class="panel-footer ">
                            <p>آموزش حل معادلات لاپلاس به روش آسان آموزش حل معادلات لاپلاس به روش آسان</p>
                            <button class="btn btn-danger">نمایش</button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">آیتم اول</div>
                        <div class="panel-body"><img src="/img/icon-1.png" class="img-responsive" style="width:100%"
                                alt="Image"></div>
                        <div class="panel-footer ">
                            <p>آموزش حل معادلات لاپلاس به روش آسان آموزش حل معادلات لاپلاس به روش آسان</p>
                            <button class="btn btn-danger">نمایش</button>
                        </div>
                    </div>
                </div>
            </div>
        </div><br>

        <div class="container" dir="rtl">
            <div class="row">
                <div class="col-sm-4 ">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">آیتم اول</div>
                        <div class="panel-body"><img src="/img/icon-1.png" class="img-responsive" style="width:100%"
                                alt="Image"></div>
                        <div class="panel-footer ">
                            <p>آموزش حل معادلات لاپلاس به روش آسان آموزش حل معادلات لاپلاس به روش آسان</p>
                            <a href="/project/Detail/1"><button class="btn btn-danger">نمایش</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">آیتم اول</div>
                        <div class="panel-body"><img src="/img/icon-1.png" class="img-responsive" style="width:100%"
                                alt="Image"></div>
                        <div class="panel-footer ">
                            <p>آموزش حل معادلات لاپلاس به روش آسان آموزش حل معادلات لاپلاس به روش آسان</p>
                            <button class="btn btn-danger">نمایش</button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">آیتم اول</div>
                        <div class="panel-body"><img src="/img/icon-1.png" class="img-responsive" style="width:100%"
                                alt="Image"></div>
                        <div class="panel-footer ">
                            <p>آموزش حل معادلات لاپلاس به روش آسان آموزش حل معادلات لاپلاس به روش آسان</p>
                            <button class="btn btn-danger">نمایش</button>
                        </div>
                    </div>
                </div>
            </div>
            
        </div><br><br>
    </div>
    <br><br>
    <div>
        <center>
            <div dir="rtl">
                <h2 style="color: dodgerblue">---- آموزش های مهم ----</h2>
            </div>

        </center>
        <div class="container" dir="rtl">
            <div class="row">
                <div class="col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">آیتم اول</div>
                        <div class="panel-body"><img src="" class="img-responsive" style="width:100%" alt="Image"></div>
                        <div class="panel-footer">توضیحات</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-danger">
                        <div class="panel-heading">آیتم دوم</div>
                        <div class="panel-body"><img src="" class="img-responsive" style="width:100%" alt="Image"></div>
                        <div class="panel-footer">توضیحات</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">ایتم سوم</div>
                        <div class="panel-body"><img src="" class="img-responsive" style="width:100%" alt="Image"></div>
                        <div class="panel-footer">توضیحات</div>
                    </div>
                </div>
            </div>
        </div><br>

        <div class="container" dir="rtl">
            <div class="row">
                <div class="col-sm-4">
                    <div class="panel panel-warning">
                        <div class="panel-heading">یسمش</div>
                        <div class="panel-body"><img src="" class="img-responsive" style="width:100%" alt="Image"></div>
                        <div class="panel-footer">مشسی</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-info">
                        <div class="panel-heading">مسشی</div>
                        <div class="panel-body"><img src="" class="img-responsive" style="width:100%" alt="Image"></div>
                        <div class="panel-footer">نسشی</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">یمسش</div>
                        <div class="panel-body"><img src="" class="img-responsive" style="width:100%" alt="Image"></div>
                        <div class="panel-footer">میسش</div>
                    </div>
                </div>
            </div>
        </div><br><br>
    </div>
    <br><br>
    <div>
        <center>
            <div dir="rtl">
                <h2 style="color: dodgerblue">---- آموزش های مهم ----</h2>
            </div>

        </center>
        <div class="container" dir="rtl">
            <div class="row">
                <div class="col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">آیتم اول</div>
                        <div class="panel-body"><img src="" class="img-responsive" style="width:100%" alt="Image"></div>
                        <div class="panel-footer">توضیحات</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-danger">
                        <div class="panel-heading">آیتم دوم</div>
                        <div class="panel-body"><img src="" class="img-responsive" style="width:100%" alt="Image"></div>
                        <div class="panel-footer">توضیحات</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">ایتم سوم</div>
                        <div class="panel-body"><img src="" class="img-responsive" style="width:100%" alt="Image"></div>
                        <div class="panel-footer">توضیحات</div>
                    </div>
                </div>
            </div>
        </div><br>

        <div class="container" dir="rtl">
            <div class="row">
                <div class="col-sm-4">
                    <div class="panel panel-warning">
                        <div class="panel-heading">یسمش</div>
                        <div class="panel-body"><img src="" class="img-responsive" style="width:100%" alt="Image"></div>
                        <div class="panel-footer">مشسی</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-info">
                        <div class="panel-heading">مسشی</div>
                        <div class="panel-body"><img src="" class="img-responsive" style="width:100%" alt="Image"></div>
                        <div class="panel-footer">نسشی</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">یمسش</div>
                        <div class="panel-body"><img src="" class="img-responsive" style="width:100%" alt="Image"></div>
                        <div class="panel-footer">میسش</div>
                    </div>
                </div>
            </div>
        </div><br><br>
    </div>
    <br><br><br>





    <script>
        window.onscroll = function () {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) {
                $("#nav_menu").show();
            } else {
                $("#nav_menu").hide();
            }
        }
    </script>

</body> --}}





</html>