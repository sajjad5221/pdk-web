@extends('layouts.main')

<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ"
        crossorigin="anonymous">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</head>

<body>
    <div class="user-type  " dir="rtl">
        <div class="container">
            <div class="col-md-3 gold ">
                <i style="margin-top:100px;" class="fas fa-crown fa-4x "></i>
                <p class="member">عضویت طلایی</p>
                <p class="price">600,000 ریال ماهیانه
                </p>
                <p class="description">
                    اعضای طلایی پروفایل حرفه ای دریافت می‌کنند، نشان عضویت طلایی می‌گیرند و می‌توانند 240 پیشنهاد در
                    ماه ارسال کنند
                </p>
                <button class="btn btn-default upgrade-btn">ارتقا عضویت </button>
            </div>
            <div class="col-md-3 silver ">
                    <i style="margin-top:100px;" class="fas fa-crown fa-4x "></i>
                    <p class="member" style="color:gray">عضویت نقره ای                        </p>
                    <p class="price">400,000 ریال ماهیانه 

                    </p>
                    <p class="description">
                        
اعضای نقره ای پروفایل حرفه ای دریافت می‌کنند، نشان عضویت نقره ای می‌گیرند و می‌توانند 120 پیشنهاد در ماه ارسال کنند
                    </p>
                    <button class="btn btn-default upgrade-btn">ارتقا عضویت </button>
                </div>
                <div class="col-md-3 bronze ">
                        <i style="margin-top:100px;" class="fas fa-crown fa-4x "></i>
                        <p class="member">عضویت برنزی</p>
                        <p class="price">200,000 ریال ماهیانه 
                        </p>
                        <p class="description">
                                اعضای برنزی پروفایل حرفه ای دریافت می‌کنند، نشان عضویت برنزی می‌گیرند و می‌توانند 60 پیشنهاد در ماه ارسال کنند
                        </p>
                        <button class="btn btn-default upgrade-btn">ارتقا عضویت </button>
                    </div>
                    <div class="col-md-3 normal ">
                            <i style="margin-top:100px;" class="fas fa-coffee fa-4x "></i>
                            <p class="member">عضویت معمولی</p>
                            <p class="price">رایگان
                            </p>
                            <p class="description">
                                    اعضای معمولی میتوانند در مسابقات شرکت کنند و 5 پیشنهاد  ارسال کنند

                            </p>
                        </div>
        </div>
    </div>
</body>

</html>