

@extends('layouts.main')
<html dir="rtl">

<head>
    <link href="/css/style.css" rel="stylesheet">
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <link rel="stylesheet" href="/plugin/quill/quill.snow.css" >

</head>
<body>
        <div class="row project-title">
                <div dir="rtl">
                    <h1 class="name">پیاده سازی اپلیکیشن فروشگاهی برای اندروید</h1>
                    <h3 class="create">ایجاد شده توسط <a href="/users/#">dhakimi</a></h3>

                </div>
            </div>
<div class="wrapper">

        
        <div class="image-gallery">
          <aside class="thumbnails">
            <a href="#" class="selected thumbnail" data-big="http://placekitten.com/420/600">
              <div class="thumbnail-image" style="background-image: url(http://placekitten.com/420/600)"></div>
            </a>
            <a href="#" class="thumbnail" data-big="http://placekitten.com/450/600">
              <div class="thumbnail-image" style="background-image: url(http://placekitten.com/450/600)"></div>
            </a>
            <a href="#" class="thumbnail" data-big="http://placekitten.com/460/700">
              <div class="thumbnail-image" style="background-image: url(http://placekitten.com/460/700)"></div>
            </a>
          </aside>
        
          <main class="primary" style="background-image: url('http://placekitten.com/420/600');"></main>
        </div>
        
        </div>
        <div class="project">
                
                <div class="row" dir="rtl">
                        <div class="col-md-5">
                            <ul class="show-price">
                                    <li>قیمت محصول</li>
                                    <li>300,000 ریال </li>
                                        
                
                                </ul>
                                <ul class="show-price">
                                        <li>دسته بندی های مرتبط با این محصول</li>
                                        <li>
                                            <a href="#" class="ability">برنامه نویسی موبایل</a> <a href="#" class="ability">php</a>
                                        </li>
                    
                                    </ul>
                            </div>
                    <div class="project-description">
                            <div class="col-md-7">
        
                        <h3>توضیحات محصول</h3>
                            <h4>یک اپلیکیشن موبایل فروشگاهی طراحی اولیه و cms دارد و نیاز به پیاده سازی روی موبایل اندروید را
                                دارد. لطفا بدون رزومه قبلی و نمونه کار معرفی نگذارید. جزئیات پروژه با فریلنسر منتخب در میان
                                گزاشته میشود.</h4>
                        </div>
                    </div>
                   
                </div>
                <hr class="line">
                <div class="show-data">
                    <div class="row" >
                        <div class="col-md-6"><h3>همه پروژه ها</h3></div>
                        <div class="col-md-6"><h3>همه کاربران</h3></div>
                    </div>
                    <div class="row" >
                            <div class="col-md-6"><h3>14200</h3></div>
                            <div class="col-md-6"><h3>22500</h3></div>
                        </div>
                </div>
            </div>
</body>
<script>
    $('.thumbnail').on('click', function() {
  var clicked = $(this);
  var newSelection = clicked.data('big');
  var $img = $('.primary').css("background-image","url(" + newSelection + ")");
  clicked.parent().find('.thumbnail').removeClass('selected');
  clicked.addClass('selected');
  $('.primary').empty().append($img.hide().fadeIn('slow'));
});
</script>
</html>