@extends('layouts.main')
<html >

<head>
    <meta charset="utf-8">
    
    <link rel="stylesheet" href="/css/bootstrap.css" >
    <link rel="stylesheet" href="/css/style.css" >
    <link rel="stylesheet" href="/js/jquery-ui-1.12.1/jquery-ui.theme.css">
    <link rel="stylesheet" href="/plugin/Input-master/dist/jquery.tagsinput.min.css">
    <link rel="stylesheet" href="/plugin/tag-it-master/css/jquery.tagit.css">
    <link rel="stylesheet" href="/plugin/quill/quill.snow.css" >

</head>

<body>
    <div class="make-project" dir="rtl">
        {{-- <form  method="POST" action="/advertising/makeAdvertising"> --}}
            <form method="POST" action="/advertising/makeAdvertising">
            {{csrf_field()}}
            <div>
                <span class=" numberCircle ">1</span>
                <span class="text-after-number">انتخاب دسته بندی تبلیغ</span>
            </div>
            <div class=" col-md-offset-5">

                <div class="form-group" style="margin-top:20px;margin-right:20px" dir="rtl">
                    <input type="text" class="form-control" id="tags" >
                </div>
            </div>
            <div>
                <span class=" numberCircle ">2</span>
                <span class="text-after-number"> عنوان تبلیغ</span>
            </div>
            <div class="col-md-8 col-md-offset-4">

                <div class="form-group" style="margin-top:20px;">
                    <label >نام پروژه</label>
                    <input type="text" class="form-control" name="title">
                </div>
            </div>
            <div>
                <span class=" numberCircle ">3</span>
                <span class="text-after-number">توضیحات </span>
            </div>
            <div class="col-md-8 col-md-offset-4" dir="ltr">
                    <input type="hidden" class="form-control" name="about" id="about">

                <div class="form-group"  id="editor" style="text-align:right;height:400px;" name="description">
                    <label >توضیحات </label>
                    {{-- <textarea class="form-control"  rows="5" name="description" id="editor"></textarea> --}}
                    {{-- <div id="editor"></div> --}}
                </div>
            </div>
           
            <div>
                <span class=" numberCircle ">5</span>
                <span class="text-after-number">بارگذاری عکس ها</span>
            </div>
            <div class="col-md-8 col-md-offset-4 file-upload">

                <div class="form-group" style="margin-top:20px;">
                    <input type="file" class="form-control" name="files" id="files">
                </div>
            </div>
            <div class="col-md-2 col-md-offset-6">

                <div class="form-group" style="margin-top:20px;">

                    <button class="form-control btn btn-success"   >ثبت تبلیغ</button>
                </div>
            </div>
            <input type="hidden" class="form-control" name="selectedTags" id="selectedTags">

        </form>

          {{-- <div id="editor"></div> --}}
          
    </div>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/plugin/Input-master/dist/jquery.tagsinput.min.js"></script>
    <script src="/plugin/tag-it-master/js/tag-it.min.js"></script>
    <script src="/plugin/tag-it-master/js/tag-it.min.js"></script>
    <script src="/plugin/quill/quill.min.js"></script>
    
    {{-- <link rel="stylesheet" href="/plugin/Input-master/dist/jquery.tagsinput.min.js"> --}}

    <script>
            $(document).ready(function() {
              var tags = [];
              var selectedTags = [];
              var categoriesObject = ({!!$categories!!});
              for(var i=0;i<categoriesObject.length;i++)
              {
                tags.push(categoriesObject[i].name);
              }
             $('#tags').tagsInput({
                autocomplete:{width:'20px'},
                'autocomplete_url': tags,
                'onAddTag':function(){
                 selectedTags=(jQuery( '#tags' ).tagsInput()[0].value);
                 console.log(selectedTags);

                    },
                'onRemoveTag':function(){
                 selectedTags=(jQuery( '#tags' ).tagsInput()[0].value);
                 console.log(selectedTags);
                },
                'onChange':function(){
                 $("#selectedTags").val(selectedTags);
                },
                'defaultText':'',
                'placeholderColor' : 'black',
                'width':'100%',
                'height':'50px',
             });

            var quill = new Quill('#editor', {
                 theme: 'snow'
            });  
            quill.format('direction', 'rtl');
            quill.format('align', 'right');


            var form = document.querySelector('form');
            form.onsubmit = function() {
            var about = $('#about');
            about.value = JSON.stringify(quill.getContents());
            $("#about").val(about.value)

            //console.log("Submitted", $(form).serialize(), $(form).serializeArray(),about);
            console.log(about.value);
            
            /*$.ajax({
                type: "POST",
                url: '/advertising/makeAdvertising',
                data: { id: about.value },
                success: function() {
                alert(about.value);
                }
                  });*/
                  //return false;

                }

             });

    </script>
</body>

</html>