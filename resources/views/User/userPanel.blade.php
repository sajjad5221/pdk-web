@extends('layouts.main')
@section('content')
  <div class="col-md-2 pull-right">
    <div class="panel panel-default">
      <div class="panel-heading">Panel </div>
      <div class="panel-body">
        <table class="table">
            <tbody>
              <tr><td><a href="showProfile/{{Auth::user()->id}}">profile</a></td></tr>
              <tr><td><a href="changePassword">Change Password</a></td></tr>
              <tr><td><a href="showApplicantProject/{{Auth::user()->id}}">لیست پروژه های دریافت کرده</a></td></tr>
              <tr><td><a href="showGivenProject/{{Auth::user()->id}}">لیست پروژه های واگذار کرده</a></td></tr>
            </tbody>
        </table>


      </div>
    </div>
  </div>
@endsection
