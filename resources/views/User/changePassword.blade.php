@extends('layouts.main')
@section('content')
<form method="POST" >
    {{ csrf_field() }}
    <div class="form-group">
      <label for="exampleInputEmail1">Previous Pass</label>
      <input type="password" class="form-control" name="current_password">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">New Pass</label>
      <input type="password" class="form-control" name="password">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Confrim Pass</label>
        <input type="password" class="form-control" name="password_confirmation">
      </div>
    <button type="submit" class="btn btn-primary" >Edit</button>
  </form>
@endsection
