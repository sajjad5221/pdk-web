@extends('layouts.userPanel')
<html>

<head>
    <link href="/css/style.css" rel="stylesheet">
</head>

<body>
    <div class="row">
            <div class="show-icon col-md-offset-7 ">
                    <span class="text-icon">داوطلب های پروژه</span>
                    <i class="fas fa-briefcase fa-2x"></i>
                </div>
    </div>

    <div class="col-md-8 col-sm-10 col-md-offset-1 card-box">
        <div class="card">
            
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">نام کاربر</th>
                        <th scope="col">مبلغ پیشنهادی</th>
                        <th scope="col">زمان پیشنهادی</th>
                    </tr>
                </thead>
                @foreach($volunteers as $volunteer)

                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td><a href="users/{{$volunteer->id}}" >{{$volunteer->name}}</td>
                        <td>{{$volunteer->suggested_price}}</td>
                        <td>{{$volunteer->suggested_day}}</td>
                        <td><a><button class="btn btn-defualt">مشاهده</button></a>
                    </tr>

                </tbody>
                @endforeach
            </table>
        </div>
    </div>
</body>

</html>