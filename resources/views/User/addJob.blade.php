@extends('layouts.main')
@section('content')
<form method="POST" >
    {{ csrf_field() }}
    <div class="form-group">
      <label for="exampleInputEmail1">Title</label>
      <input type="text" class="form-control" name="title">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Description</label>
      <input type="text" class="form-control" name="description">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Amount</label>
        <input type="text" class="form-control" name="amount">
      </div>
      
    <button type="submit" class="btn btn-primary" name="action" value="addProject">Submit</button>
  </form>
@endsection
