@extends('layouts.userPanel')
<html>

<head>
    <link href="/css/style.css" rel="stylesheet">
</head>

<body>
    <div class="row">
            <div class="show-icon col-md-offset-7 ">
                    <span class="text-icon">داوطلبان پروژه </span>
                    <i class="fas fa-briefcase fa-2x"></i>
                </div>
    </div>

    <div class="col-md-8 col-sm-10 col-md-offset-1 card-box">
        <div class="card">
            
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">نام درخواست دهنده</th>
                    </tr>
                </thead>
                @foreach($volunteers as $volunteer)

                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td><a href="/users/{{$volunteer->user_id}}">{{$volunteer->name}}</a></td>
                        <td><input type="submit" class="btn btn-success" onclick="acceptSubmit({{$volunteer->user_id}});" value="پذیرفتن درخواست"></input>

                    </tr>

                </tbody>
                @endforeach
            </table>
        </div>
    </div>
    <script>
function acceptSubmit(user_id){
    var r = confirm("آیا از انتخاب خود مطمين هستید؟");
    if (r == true) {
        
        window.location.href = 'acceptRequest/'+user_id;
    } else {
        return false;
    }
    console.log(txt);
}
        </script>
</body>

</html>