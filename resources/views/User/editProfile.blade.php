@extends('layouts.main')
@section('content')
<form method="POST" >
    {{ csrf_field() }}
    <div class="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="{{Auth::user()->email}}" name="email">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Name</label>
      <input type="text" class="form-control" id="name" placeholder="{{Auth::user()->name}}" name="name">
    </div>
   
    <button type="submit" class="btn btn-primary">Edit</button>
  </form>
@endsection
