@extends('layouts.userPanel')
@extends('layouts.main')
<html>

<head>
    <link href="/css/style.css" rel="stylesheet">
</head>

<body>
    <div class="row">
            <div class="show-icon col-md-offset-7 ">
                    <span class="text-icon">پروژه ها</span>
                    <i class="fas fa-briefcase fa-2x"></i>
                </div>
    </div>

    <div class="col-md-8 col-sm-10 col-md-offset-1 card-box">
        <div class="card">
            
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">نام پروژه</th>
                        <th scope="col">کارفرما</th>
                    </tr>
                </thead>
                @foreach($g_projects as $g_project)

                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>{{$g_project->title}}</td>
                        <td><a href="/users/{{$g_project->employer_id}}">{{$g_project->name}}</a></td>
                    <td><a href="/project/Detail/{{$g_project->id}}" ><button class="btn btn-defualt">نمایش</button></a>
                    </tr>

                </tbody>
                @endforeach
            </table>
        </div>
    </div>
</body>

</html>