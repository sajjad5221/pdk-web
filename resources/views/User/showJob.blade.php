@extends('layouts.userPanel')
<html>

<head>
    <link href="/css/style.css" rel="stylesheet">
</head>

<body>
    <div class="row">
            <div class="show-icon col-md-offset-7 ">
                    <span class="text-icon">پروژه ها</span>
                    <i class="fas fa-briefcase fa-2x"></i>
                </div>
    </div>

    <div class="col-md-8 col-sm-10 col-md-offset-1 card-box">
        <div class="card">
            {{--
            <h4>
                <b>Jane Doe</b>
            </h4>
            <p>Interior Designer</p> --}}
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">عنوان شغل</th>
                        <th scope="col">واگذار کننده</th>
                        <th scope="col">وضعیت</th>
                    </tr>
                </thead>
                @foreach($jobs as $job)

                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>{{$job->title}}</td>
                        <td>{{$job->description}}</td>
                    <td><a href="/job/Detail/{{$job->id}}" ><button class="btn btn-defualt">نمایش</button></a>
                    </tr>

                </tbody>
                @endforeach
            </table>
        </div>
    </div>
</body>

</html>