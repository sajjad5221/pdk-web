@extends('layouts.main')
<head>
    <link href="/css/style.css" rel="stylesheet">
    <meta charset="utf-8">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/bootstrap.js"></script>
</head>

<body>
    <div class="project">
        <div class="row project-title">
            <div dir="rtl">
                <h1 class="name">کار در سازمان هواشناسی</h1>
                <h3 class="create">ایجاد شده توسط <a href="/users/#">dhakimi</a></h3>
                <div class="send-suggest">
                    <a href="/job/sendApply/{{$id}}"><button class="btn btn-danger " style="font-size:25px;">ارسال پیشنهاد</button></a>
                </div>
            </div>
        </div>
        <div class="row" dir="rtl">
                <div class="col-md-5">
                    <ul class="show-price">
                            <li>حقوق ماهیانه</li>
                            <li>300,000ریال - 2,500,000ریال</li>
        
                        </ul>
                        <ul class="show-price">
                                <li>مهارت های مورد نیاز</li>
                                <li>
                                    <a href="#" class="ability">برنامه نویسی موبایل</a> <a href="#" class="ability">php</a>
                                </li>
            
                            </ul>
                    </div>
            <div class="project-description">
                    <div class="col-md-7">

                <h3>توضیحات</h3>
                    <h4>یک اپلیکیشن موبایل فروشگاهی طراحی اولیه و cms دارد و نیاز به پیاده سازی روی موبایل اندروید را
                        دارد. لطفا بدون رزومه قبلی و نمونه کار معرفی نگذارید. جزئیات پروژه با فریلنسر منتخب در میان
                        گزاشته میشود.</h4>
                </div>
            </div>
           
        </div>
        <hr class="line">
        <div class="show-data">
            <div class="row" >
                <div class="col-md-6"><h3>همه شغل ها</h3></div>
                <div class="col-md-6"><h3>همه کاربران</h3></div>
            </div>
            <div class="row" >
                    <div class="col-md-6"><h3>14200</h3></div>
                    <div class="col-md-6"><h3>22500</h3></div>
                </div>
        </div>
    </div>
</body>