<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ"
        crossorigin="anonymous">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/script.js"></script>
</head>

<body>

    <div class="user-panel">
        <div class="row">

            <div class="col-md-7  profile-description">
            <p dir="rtl">سجاد مومنی</br>عضویت معمولی  <a><i class="fa fa-plus " style="color:green"></i></a></p>
            
            </div>
            <div class="col-md-3 profile-image">
                    <img class="img-circle " src="/img/sajjad-momeni.jpg" width="60px" height="60px" style="display:inline-block">
                </div>
        </div>
            <ul>
                    {{-- <li class="profile-image">
                            <ul class="profile-description">
                                <li>
                                    <a>سجاد مومنی</a>
                                    <a>سجاد مومنی</a>
                                </li>
                            </ul>
                        </li> --}}
                <li>
                    <a href="/user/showApplicantProject">پروژه
                        <i class="fas fa-user fa-lg"></i>
                    </a>
                </li>
                <li>
                    <a href="/user/showJob">شغل ها
                        <i class="fas fa-user fa-lg"></i>
                    </a>
                </li>
                <button class="dropdown-btn">
                    <i class="fa fa-caret-down"></i>
                    آموزش ها
                    <i class="fas fa-user fa-lg"></i>
                </button>
                <div class="dropdown-container">
                    <li><a href="#">آموزش های شما</a></li>
                    <li><a href="#">آموزش ۲ </a></li>
                </div>
            </ul>
        </div>
</body>

</html>