<html>
<head>
	<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<style>
body {
  font-family: Helvetica, sans-serif;
  font-size:15px;
}

a {
  text-decoration:none;
}
ul.tree, .tree li {
    list-style: none;
    margin:0;
    padding:0;
    cursor: pointer;
}

.tree ul {
  display:none;
}

.tree > li {
  display:block;
  background:#eee;
  margin-bottom:2px;
}

.tree span {
  display:block;
  padding:10px 12px;

}

.icon {
  display:inline-block;
}

.tree .hasChildren > .expanded {
  background:#999;
}

.tree .hasChildren > .expanded a {
  color:#fff;
}

.icon:before {
  content:"+";
  display:inline-block;
  min-width:20px;
  text-align:center;
}
.tree .icon.expanded:before {
  content:"-";
}

.show-effect {
  display:block!important;
}
</style>
</head>
</body>
<div class="col-md-offset-3">
<ul class="tree">

	<li class="tree__item">
		<span>
	        <a href="#">فیلترینگ</a>
		</span>

	</li>

	<li class="tree__item hasChildren">
		<span>
			<input type="checkbox" class="check" >کامپیوتر
		</span>

		<ul>
			<li>
				<span>
					<input type="checkbox" class="check">برنامه نویسی
				</span>

				<ul>
					<li>
						<span><a href="">سی</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">سخت افزار</a></span>
			</li>

			<li>
				<span><a href="#">علوم</a></span>
			</li>

		</ul>

	</li>

	<li class="tree__item hasChildren">

		<span>
	        <div class="check"></div>
			<a href="#">برق</a>
		</span>

		<ul>
			<li>
				<span><a href="#">Acting</a></span>
			</li>

			<li>
				<span><a href="#">Biomechanics</a></span>
			</li>

			<li>
				<span><a href="#">Improvisation</a></span>
			</li>

		</ul>

	</li>
</ul>
</div>
<script>
$('.tree .check').click( function() {
  $(this).parent().toggleClass('expanded').
  closest('li').find('ul:first').
  toggleClass('show-effect');
});
</script>
</body>
</html>
