<!doctype html>
<html >

<head>
	<meta charset="utf-8">
	<link href="/css/bootstrap.css" rel="stylesheet">
	<link href="/css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<script src="/js/bootstrap.js"></script>


</head>

<body>
	<div class="topnav">
			<li><a href="#home"><img src="/img/logo.png" width="80px" height="80px"></img></a></li>

		<div class="navbar-content">
			{{-- <li > <a href="#1" style="">پروژه ها</a></li>
			<li> <a href="#2">پروژه ها</a></li>
			<li> <a href="#3">پروژه ها</a></li> --}}
			<li>
				<div class="dropdown" >

					<button class="dropbtn">پروژه ها
						<i class="fa fa-caret-down"></i>
					</button>

					<div class="dropdown-content">
						<ul >
							<li ><a href="/project/showAll" style="font-size:12px;font-weight: bold;">لیست پروژه ها</a></li>
							<li><a href="/project/makeProject" style="font-size:12px;font-weight: bold;">ساخت پروژه</a></li>
						</ul>
					</div>
				</div>
			</li>
			<li>
					<div class="dropdown" >
	
						<button class="dropbtn">آموزش ها
							<i class="fa fa-caret-down"></i>
						</button>
	
						<div class="dropdown-content">
							<ul >
								<li ><a href="/tutorial/showAll" style="font-size:12px;font-weight: bold;">لیست آموزش ها</a></li>
								<li><a href="/tutorial/makeTutorial" style="font-size:12px;font-weight: bold;">ساخت آموزش </a></li>
							</ul>
						</div>
					</div>
				</li>
				<li>
					<div class="dropdown" >
	
						<button class="dropbtn">تبلیغات 
							<i class="fa fa-caret-down"></i>
						</button>
	
						<div class="dropdown-content">
							<ul >
								<li ><a href="/advertising/showAllAdvertising" style="font-size:12px;font-weight: bold;">لیست تبلیغات</a></li>
								<li><a href="/advertising/makeAdvertising" style="font-size:12px;font-weight: bold;">ساخت تبلیغ </a></li>
							</ul>
						</div>
					</div>
				</li>
			
					<li>
						<div class="dropdown" >
		
							<button class="dropbtn">شغل ها
								<i class="fa fa-caret-down"></i>
							</button>
		
							<div class="dropdown-content">
								<ul >
									<li ><a href="/job/showAll" style="font-size:12px;font-weight: bold;">لیست شغل ها</a></li>
									<li><a href="/job/makeJob" style="font-size:12px;font-weight: bold;">ساخت شغل </a></li>
								</ul>
							</div>
						</div>
					</li>
		</div>
	</div>
	<footer class="footer-distributed" style="font-family:'Vazir'">

		<div class="footer-left">


			<p class="footer-links">
				<a href="#">خانه</a>
				/
				<a href="#">پروژه ها</a>
				/
				<a href="#">تبلیغات</a>
				/
				<a href="#">شغل ها</a>
				/
				<a href="#">آموزش ها</a>
				/
			</p>

			<p class="footer-company-name">&copy; پیک دانش</p>
		</div>

		<div class="footer-center">

			<div>
				<i class="fa fa-map-marker"></i>
				<p><span>چهار راه فردوسی</span> چهارمحال و بختیاری, شهرکرد</p>
			</div>

			<div>
				<i class="fa fa-phone"></i>
				<p>038-4502156</p>
			</div>

			<div>
				<i class="fa fa-envelope "></i>
				<p><a class="contact-us-mail" href="mailto:peyk_danesh@gamil.com">peyk_danesh@gamil.com</a></p>
			</div>

		</div>

		<div class="footer-right">

			<p class="footer-company-about">
				<span>درباره ما</span>
				پیک دانش استارتاپی چند پاره است که میخواهد فریلنسر ها را دور هم در یکچا جمع کند
			</p>

			<div class="footer-icons">

				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-linkedin"></i></a>
				<a href="#"><i class="fa fa-github"></i></a>

			</div>

		</div>

	</footer>
</body>




</html>