@extends('layouts.main')

<html>

<head>

    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/style.css">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">


</head>

<body>
    <div class="apply-project container">
        <div class="row price-day">
            <div class="col-md-5 ">
                <p style="text-align: right;">مدت زمان تحویل پروژه</p>
            </div>
            <div class="col-md-5">
                <p style="text-align: right;">هزینه تحویل پروژه</p>
            </div>
        </div>
        <form method="POST">
            {{ csrf_field() }}

            <div class="form-inline ">
                <div class="input-group  col-md-3 col-md-offset-2">
                    <span class="input-group-addon">روز</span>

                    <input class="form-control" type="number" name="day" placeholder="تعداد روز">
                </div>
                <div class="input-group  col-md-3 col-md-offset-2">
                    <span class="input-group-addon">IRR</span>
                    <input class="form-control" type="number" name="price" placeholder="هزینه به ریال">
                </div>
            </div>
            <hr>
            <div class="row" dir="rtl">
                <div class="col-md-4 col-md-offset-1 description-description">
                    <p>پیشنهاد شما ممکن است اولین و تنها شانس شما برای جلب توجه کارفرما باشد.</br>
                        لطفا به نکات زیر دقت کنید:
                    </p>
                    <ul>
                        <li>دقت کنید چه کاری خواسته شده.</li>
                        <li>توضیح دهید چطور تجربیات و مهارت های شما کمک می کند شما بهترین فرد برای انجام این پروژه
                            باشید.</li>
                        <li>توضیح دهید چطور می خواهید کار را انجام دهید</li>
                    </ul>
                </div>
                <div class="col-md-5 col-md-offset-1">
                    <label for="comment" class="description">توضیحات:</label>
                    <textarea class="form-control" rows="7" name="description"></textarea>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-offset-5">
                    <button class="btn btn-success">ثبت پیشنهاد</button>
                    <button class="btn btn-default">لغو</button>
                </div>
            </div>
        </form>

    </div>

</body>

</html>