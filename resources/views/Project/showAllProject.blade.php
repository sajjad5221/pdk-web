@extends('layouts.main')
<html >

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/bootstrap.css" >
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css" >
    <link rel="stylesheet" href="/js/jquery-ui-1.12.1/jquery-ui.theme.css">
    <link rel="stylesheet" href="/plugin/Input-master/dist/jquery.tagsinput.min.css">
    <link rel="stylesheet" href="/plugin/tag-it-master/css/jquery.tagit.css">

</head>
<body >
    <div class="container" >
<div class="col-md-12 search-box" style="margin-top:10%;">
        <div class="row price-day">

                <div class="col-md-7">
                    <p style="text-align: right;">جست و جو بر اساس مهارت ها</p>
                </div>
            </div>
            <form method="POST">
                {{ csrf_field() }}
    
                <div class="form-inline ">
                    <div class="input-group  col-md-3 col-md-offset-4">
                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        <input class="form-control" type="text" name="tags" id="tags" placeholder="یک حرف وارد کنید">
                        {{-- <input class="form-control" type="text" name="search" placeholder="یک حرف وارد کنید"> --}}
                    </div>
                </div>
                <div class="input-group  col-md-3 col-md-offset-4">
                    
                    <button class="btn btn-default" >Search</button>
                        {{-- <input class="form-control" type="text" name="search" placeholder="یک حرف وارد کنید"> --}}
                </div>
            </form>
</div>
    </div>
    @foreach($projects as $project)
    <div class="row "dir="rtl">
    <a href="/project/Detail/{{$project->id}}">
        <div class="project-box col-md-offset-1" style="margin-top: 10%;">
            <div class="box-description">
                <div class="project-header col-md-8">
                        <h2>{{$project->title}}</h2>
                    </br>
                        <p style="text-align:right">
                            نمونه مشابه متن انگلیسی آپلود شده است.
                                متن اصلی شامل 237000 لغت می باشد.
                                
                                حداکثر تا 20 روز پروژه تحویل شود.
                                
                                قیمت پیشنهادی 700 هزار تومان تا 1 میلیون تومان
                        </p>
                    </br>
                    <a href="#" class="ability">برنامه نویسی موبایل</a> <a href="#" class="ability">php</a>

                    {{-- @foreach (($project->tags) as $tags)
                    {{($tags)}}
                    @endforeach --}}
                    
                </div>
            </br>
                
            </div>
            <div class="box col-md-2 ">
                <div class="box-price"><p>بودجه:1500000 ریال</div>
            <i class="fas fa-clipboard-list fa-6x"></i>
        </div>
        </div>
    </div>
    </a>
    <hr>
    
   @endforeach
   <div class="show-pagination">
   {{ $projects->links() }}
   </div>


   <script src="/js/jquery-3.3.1.min.js"></script>
   <script src="/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
   <script src="/js/bootstrap.js"></script>
   <script src="/plugin/Input-master/dist/jquery.tagsinput.min.js"></script>
   <script src="/plugin/tag-it-master/js/tag-it.min.js"></script>
   <script>
            $(document).ready(function() {
              var tags = [];
              var selectedTags = [];
              var categoriesObject = ({!!$categories!!});
              for(var i=0;i<categoriesObject.length;i++)
              {
                tags.push(categoriesObject[i].name);
              }
             $('#tags').tagsInput({
                autocomplete:{width:'100px'},
                'autocomplete_url': tags,
                'onAddTag':function(){
                 selectedTags=(jQuery( '#tags' ).tagsInput()[0].value);
                 console.log(selectedTags);

                    },
                'onRemoveTag':function(){
                 selectedTags=(jQuery( '#tags' ).tagsInput()[0].value);
                 console.log(selectedTags);
                },
                'onChange':function(){
                 $("#selectedTags").val(selectedTags);
                },
                'defaultText':'',
                'placeholderColor' : 'black',
                'width':'100%',
                'height':'50px',
             });


        });
    </script>
</body>

</html>