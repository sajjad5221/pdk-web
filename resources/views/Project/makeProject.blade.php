@extends('layouts.main')
<html >

<head>
    <meta charset="utf-8">
    
    <link rel="stylesheet" href="/css/bootstrap.css" >
    <link rel="stylesheet" href="/css/style.css" >
    <link rel="stylesheet" href="/js/jquery-ui-1.12.1/jquery-ui.theme.css">
    <link rel="stylesheet" href="/plugin/Input-master/dist/jquery.tagsinput.min.css">
    <link rel="stylesheet" href="/plugin/tag-it-master/css/jquery.tagit.css">

</head>

<body>
    <div class="make-project " dir="rtl">
        <form  method="POST" action="/project/makeProject">
            {{csrf_field()}}
            <div>
                <span class=" numberCircle ">1</span>
                <span class="text-after-number">انتخاب دسته بندی پروژه</span>
            </div>
            <div class=" col-md-offset-5">

                <div class="form-group" style="margin-top:20px;margin-right:20px" dir="rtl">
                    <input type="text" class="form-control" id="tags" >
                </div>
            </div>
            <div>
                <span class=" numberCircle ">2</span>
                <span class="text-after-number">انتخاب نام پروژه</span>
            </div>
            <div class="col-md-8 col-md-offset-4">

                <div class="form-group" style="margin-top:20px;">
                    <label >نام پروژه</label>
                    <input type="text" class="form-control" name="title">
                </div>
            </div>
            <div>
                <span class=" numberCircle ">3</span>
                <span class="text-after-number">توضیحات پروژه</span>
            </div>
            <div class="col-md-8 col-md-offset-4">

                <div class="form-group" style="margin-top:20px;">
                    <label >توضیحات </label>
                    <textarea class="form-control"  rows="5" name="description"></textarea>
                </div>
            </div>
            <div>
                <span class=" numberCircle ">4</span>
                <span class="text-after-number">هزینه پروژه</span>
            </div>
            <div class="col-md-8 col-md-offset-4">

                <div class="form-group" style="margin-top:20px;">
                    <label >هزینه به ریال</label>
                    <input type="number" class="form-control" name="price">
                </div>
            </div>
            <div>
                <span class=" numberCircle ">5</span>
                <span class="text-after-number">بارگذاری فایل ها</span>
            </div>
            <div class="col-md-8 col-md-offset-4 file-upload">

                <div class="form-group" style="margin-top:20px;">
                    <input type="file" class="form-control" name="files" id="files">
                </div>
            </div>
            <div class="col-md-2 col-md-offset-6">

                <div class="form-group" style="margin-top:20px;">

                    <button class="form-control btn btn-success" >ارسال پروژه</button>
                </div>
            </div>
            <input type="hidden" class="form-control" name="selectedTags" id="selectedTags">

        </form>
    </div>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/plugin/Input-master/dist/jquery.tagsinput.min.js"></script>
    <script src="/plugin/tag-it-master/js/tag-it.min.js"></script>
    
    {{-- <link rel="stylesheet" href="/plugin/Input-master/dist/jquery.tagsinput.min.js"> --}}
    
    <script>
            $(document).ready(function() {
              //$('#myForm').append('<input type="hidden" name="selectedTags"  />');
              var tags = [];
              var selectedTags = [];
              var categoriesObject = ({!!$categories!!});
              for(var i=0;i<categoriesObject.length;i++)
              {
                tags.push(categoriesObject[i].name);
              }
             //$('#tags').importTags('foo,bar,baz');
             $('#tags').tagsInput({
                autocomplete:{width:'20px'},
                'autocomplete_url': tags,
                'onAddTag':function(){
                 selectedTags=(jQuery( '#tags' ).tagsInput()[0].value);
                 console.log(selectedTags);

                    },
                'onRemoveTag':function(){
                 selectedTags=(jQuery( '#tags' ).tagsInput()[0].value);
                 console.log(selectedTags);
                },
                'onChange':function(){
                 $("#selectedTags").val(selectedTags);
                },
                'defaultText':'',
                'placeholderColor' : 'black',
                'width':'100%',
                'height':'50px',
             });
                /*$("#selectedTags").change(function(){
                alert("The text has been changed.");*/
            /* function postTags(){
                $.ajax({
                type: "POST",
                url: '/project/makeProject',
                data: selectedTags,
                success: function() {
                console.log("Value added");
                }
                  })
             }*/

        });
    </script>
</body>

</html>