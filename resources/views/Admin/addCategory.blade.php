<html lang="en">
<head>
  <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery UI Autocomplete - Default functionality</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    var availableTags = [];
    var categoriesObject = ({!!$categories!!});
    console.log(categoriesObject.length);
    for(var i=0;i<categoriesObject.length;i++)
    {
      var parent="";
      var id = categoriesObject[i].parent_id;
      for(var j=categoriesObject[i].id-1;categoriesObject[j].parent_id!=null;j=categoriesObject[j].parent_id-1){
         parent = categoriesObject[j].name.concat("--"+parent);
        //console.log(parent);
    }
      availableTags.push({label:parent,idx:id});
    }
      var x=null;
    $( "#tags" ).autocomplete({
      source: availableTags,
      select: function( event , ui ) {
        x = ui.item.idx;
      //alert( "You selected: " + ui.item.idx+"///"+ui.item.label );
}
    });
    document.getElementById("submit").addEventListener('click',function(){
      var text = document.getElementById('parent_id');
      text.value = x;
    })
  } );
  </script>
</head>
<body>

<div class="ui-widget">
  <form method="POST" >
    {{ csrf_field() }}
    <div class="form-group">
      <label for="tags">Tags: </label>
      <input id="tags" >
    </div>
    <div class="form-group">
      <label for="name">name: </label>
      <input id="name" name="name">
    </div>
    <div class="form-group">
      <input type="hidden" id="parent_id" name="parent_id">
    </div>
    <button type="submit" class="btn btn-primary" id="submit">Submit</button>
  </form> 
</div>

</body>
</html>