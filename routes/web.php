<?php
Route::get('/', function () {
    return view('index');
});
//------------------------------------------------Project Routing--------------------------------------------------------
Route::get('/project/Detail/{id}',"ProjectController@projectDetail");
Route::post("/project/Detail", "ProjectController@projectDetail_post");
Route::get('/project/showAll', "ProjectController@showAllProject");
Route::post('/project/showAll', "ProjectController@showAllProject_post");
Route::get('/project/sendApply/{id}', "ProjectController@sendApply");
Route::post('/project/sendApply/{id}', "ProjectController@sendApply_post");
Route::get('/project/makeProject', "ProjectController@makeProject");
Route::post('/project/makeProject', "ProjectController@makeProject_post");
//--------------------------------------------------End Project Routing---------------------------------------------------

//--------------------------------------------------Job Routing--------------------------------------------------------
Route::get('/job/showAll', "JobController@showAllJob");
Route::get('/job/Detail/{id}', "JobController@jobDetail");
Route::get('/job/sendApply/{id}', "JobController@sendApply");
Route::post('/job/sendApply/{id}', "JobController@sendApply_post");

//--------------------------------------------------End Job Routing---------------------------------------------------

//--------------------------------------------------User Routing--------------------------------------------------------
Route::get('/user/panel', "UserController@showPanel");
Route::get('/user/showProfile/{id}', "UserController@showProfile");
Route::get('/user/showApplicantProject', "UserController@showApplicantProject");
Route::get('/user/showGivenProject', "UserController@showGivenProject");
Route::get('/user/editProfile', "UserController@editProfile");
Route::post('/user/editProfile', "UserController@editProfile_post");
Route::get('/user/changePassword', "UserController@changePassword");
Route::post('/user/changePassword', "UserController@changePassword_post");
Route::get('/user/p',"UserController@userPanel");
Route::post('/user/p',"UserController@userPanel_post");
Route::get('/user/addProject',"UserController@addProject"); 
Route::post('/user/addProject',"UserController@addProject_post");
Route::get('/user/addJob',"UserController@addJob");
Route::post('/user/addJob',"UserController@addJob_post");
Route::get('/user/showJob',"UserController@showJob");
Route::get('/user/projectVolunteers/{id}',"UserController@projectVolunteers");
Route::get('/user/projectVolunteers/acceptRequest/{id}',"UserController@acceptRequest");

//--------------------------------------------------End User Routing---------------------------------------------------

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/applicant_project', 'AdminController@show_applicant_project');
Auth::routes();
Route::get('/filter', function(){
    return view('layouts.academicFilter');
});
////-----------------------------------------------Admin Routing---------------------------------------------------------
Route::get('/addCategory',"AdminController@addCategory");
Route::post('/addCategory',"AdminController@addCategory_post");





////-----------------------------------------------Advertising Routing---------------------------------------------------------
Route::get('/advertising/makeAdvertising', "AdvertisingController@makeAdvertising");
Route::post('/advertising/makeAdvertising', "AdvertisingController@makeAdvertising_post");
Route::get('/advertising/showAllAdvertising', "AdvertisingController@showAllAdvertising");
Route::get('/advertising/Detail/{id}', "AdvertisingController@advertisingDetail");

////-----------------------------------------------End Advertising Routing---------------------------------------------------------


////-----------------------------------------------Tutorial Routing---------------------------------------------------------
Route::get('/tutorial/makeTutorial', "TutorialController@makeTutorial");
Route::post('/tutorial/makeTutorial', "TutorialController@makeTutorial_post");
Route::get('/tutorial/showAll', "TutorialController@showAllTutorial");
Route::get('/tutorial/Detail/{id}',"TutorialController@tutorialDetail");
Route::post("/tutorial/Detail", "TutorialController@tutorialDetail_post");
////-----------------------------------------------End Tutorial Routing---------------------------------------------------------



////-----------------------------------------------Buy Routing---------------------------------------------------------
Route::get('/shop/selectUserType', "ShopController@selectUserType");

////-----------------------------------------------End Buy Routing---------------------------------------------------------
