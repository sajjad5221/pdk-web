<?php

use Illuminate\Database\Seeder;

class Jobs_ApplySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs_apply')->insert([
            'job_id' => rand(0,1),
            'user_id' => rand(0,1),
            'status' => rand(0,1)
            
        ]);
    }
}
