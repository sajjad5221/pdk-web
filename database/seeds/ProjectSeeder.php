<?php

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert([
            'title' => str_random(10),
            'description' => str_random(10),
            'status' => rand(0,1),
            'user_id' => rand(0,2),
            'employer_id' => rand(0,2),
        ]);
    }
}
