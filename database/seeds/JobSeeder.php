<?php

use Illuminate\Database\Seeder;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs')->insert([
            'title' => str_random(10),
            'description' => str_random(10),
            'amount' => rand(0,10000),
            'employer' => str_random(10),
        ]);
    }
}
