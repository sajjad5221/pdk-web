<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasTable('employers'))) {
            Schema::create('employers', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->timestamps();
            });
        }
        
       
        //make nullable user_id     
        //make nullable status   
        
        //add json for tags field
        if (!(Schema::hasTable('projects'))) {

        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employer_id')->unsigned();
            $table->foreign('employer_id')->references('id')->on('employers');
            $table->integer('user_id')->unsigned();
            $table->integer('price');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('title');
            $table->string('description');
            $table->binary('status');
            $table->timestamps();
        });
    }
         if (!(Schema::hasTable('jobs'))) {

         Schema::create('jobs', function (Blueprint $table) {
             $table->increments('id');
             $table->string('title');
             $table->foreign('employer_id')->references('id')->on('employers');
             //$table->string('employer');
             $table->string('description');
             $table->integer('amount');
             $table->timestamps();
         });
        }
        if (!(Schema::hasTable('categories'))) {

         Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');//need to be nullable
            $table->string('name');
            $table->timestamps();
        });
    }
    //add description 
    //add null to status
    if (!(Schema::hasTable('jobs_apply'))) {
        Schema::create('jobs_apply', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('status');

            $table->timestamps();
        });
    }
    if (!(Schema::hasTable('projects_apply'))) {
        Schema::create('projects_apply', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('jobs');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('suggested_price');
            $table->integer('suggested_day');
            $table->string('description');
            $table->timestamps();
        });
    }
    if (!(Schema::hasTable('images'))) {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path');
            $table->timestamps();
        });
    }
    //add null to image_id
    if (!(Schema::hasTable('advertising'))) {
        Schema::create('advertising', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->json('image_id');
            $table->json('tags');
            $table->string('title');
            $table->string('description');
            $table->timestamps();
        });
    }
    if (!(Schema::hasTable('tutorials'))) {
        Schema::create('tutorials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->json('tags');
            $table->string('title');
            $table->string('description');
            $table->timestamps();
        });
     }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // if (Schema::hasTable('projects')) {
        //     Schema::drop('projects');
        // }
        // if (Schema::hasTable('applicant_project')) {
        //     Schema::drop('applicant_project');
        // }
    }
}
